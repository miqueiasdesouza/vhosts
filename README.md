## Script para criação de VirtualHosts e banco de dados

by: Miquéias de Souza <miqueiasdesouza@live.com>

Use a vontade. Me ajuda bastante e espero que ajude você também. 

Por favor mantenha os direitors do criador ao redistribuir ;) 

#Instalação
para acesso global do script **server** copie o arquivo para dentro da pasta /usr/bin do seu sistema Linux




# Lista de comandos

  * -d => Nome do dominio para criação. (*) Ex: sudo server -d dominio.local

  * -p => Local da pasta projeto. Ex: sudo server -d .... -p /var/www/site

  * -c => Cria a pasta do projeto. Ex: sudo server -d ... -c

  * -rm => Remove o dominio. (*) Ex: sudo server -rm dominio.local

  * -mysql => Cria o banco de dados. Ex: sudo server -mysql banco_de_dados

  * -ms => Informa o servidor MYSQL no comando -mysql. Ex: sudo server -mysql banco_de_dados -ms localhost

  * -mu => Informa o usuário MYSQL no comando -mysql. Ex: sudo server -mysql banco_de_dados -mu root

  * -mp => Informa a senha do usuário MYSQL no comando -mysql. Ex: sudo server -mysql banco_de_dados -mp 123



 *-p não é obrigatório. Se não informado o sistema criará o vhosts com o DocumentRoot default atribuido no topo do script (PATH_HTTP_DOCS).*

*-ms, -mu e -mp podem ser definidos em constantes no topo do script => (MYSQL_SERVER, MYSQL_USER e MYSQL_PASS).*

*-mysql se usado junto com a criação do domínio -d o nome do banco de dados não precisa ser obrigatória.*

*o sistema irá pegar o nome do dominio e atribuirá ao nome do banco substituindo os pontos(.) por underline(_).*


**Comando completo para criação de vhosts com banco de dados indicando o caminho da pasta e criando a pasta**

sudo server -d dominio.local -p /var/www/PastaDoSite -c -mysql banco_de_dados

**Comando para remover o dominio**

sudo server -rm dominio.local
